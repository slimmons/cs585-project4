# CS585-Project4

CS 585 Project 4, Dr. Etzkorn, UAH

# Running the program.

Run the program using the command ```python3 project4.py```

Guess passwords, you will be able to guess passwords until you hit a password that is accepted as new.
