import re
import sys

# Takes in a password, encrypts it, and mods by 15
def encryptPassword(password):
    hashedValue = 0
    for letter in password:
        numericLetter = ord(letter)
        hashedValue += numericLetter
        hashedValue = hashedValue % 15
    return hashedValue 

# Takes in list of passwords, encrypts them, and mods each by 15
def encryptPasswords(passwords):
    encryptedPasswords = []
    for password in passwords:
        hashedValue = 0
        for letter in password:
            numericLetter = ord(letter)
            hashedValue += numericLetter
            hashedValue = hashedValue % 15
        encryptedPasswords.append(hashedValue)
    return encryptedPasswords

# Removes any lowercase values from list of passwords
def removeLowerCase(passwords):
    lowerCaseRemovedPasswords = []
    for password in passwords:
        regex = re.compile('[^A-Z]')
        password = regex.sub('', password)
        lowerCaseRemovedPasswords.append(password)
    return lowerCaseRemovedPasswords

# Gets passwords out of passwords file
def getPasswords(passwordsFile):
    with open(passwordsFile) as pf:
        passwords = pf.readlines()
    passwords = [password.strip() for password in passwords]
    passwords = removeLowerCase(passwords)
    return passwords

# Adds new password, but only used from list of passwords
def addNewPassword(password, passwordArray):
    if passwordArray[password] != 1:
        passwordArray[password] = 1
    else:
        collision = True
        counter = 1
        # case where password + counter >=14, need to check at beginning, mod counter by 15
        while collision:
            if passwordArray[password + counter] == 1:
                counter += 1
            else:
                passwordArray[password + counter] = 1
                collision = False

# Adds passwords from passwords file
def addExistingPasswords(encryptedPasswords, passwordArray):
    for password in encryptedPasswords:
        addNewPassword(password, passwordArray)

# Gets user input and performs the rest of the logic.
def getUserInput(passwordArray):
    collision = True
    while collision:
        # Get user password guess
        userGuess = input("Enter new password: ")
        regex = re.compile('[^A-Z]')
        userGuess = regex.sub('', userGuess)
        # Encrypt user password guess
        encryptedUserPassword = encryptPassword(userGuess)
        print(encryptedUserPassword)
        # If collision
        if passwordArray[encryptedUserPassword] == 1:
            print("The password you just entered is unacceptable, enter new password: ")
            collision = True
        # If no collision
        else:
            print("Your password has been changed.")
            passwordArray[encryptedUserPassword] = 1
            sys.exit()

# Files to read
passwordsFile = 'dictionary.txt'
passwordArray = [0] * 15

passwords = getPasswords(passwordsFile)

encryptedPasswords = encryptPasswords(passwords)

addExistingPasswords(encryptedPasswords, passwordArray)

getUserInput(passwordArray)
